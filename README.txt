
VIEWS FILTER STRIPPER (D7)
==========================

A simple plugin for Views to strip input of leading and trailing spaces, before
passing it on for filtering as per normal.

Automatically adds a checkbox to the Views UI of any string (text) filter on
your site, allowing the View builder to activate the above behaviour.
