<?php
/**
 * @file
 * views_filter_stripper.views.inc
 */

/**
 * Implements hook_views_data_alter().
 *
 * Changes the standard string handler to corresponding stripped string handler.
 */
function views_filter_stripper_views_data_alter(&$data) {
  foreach ($data as $table_name => $table_data) {
    foreach ($table_data as $field_name => $field_data) {
      if (isset($field_data['filter']['handler'])) {
        if ($field_data['filter']['handler'] === 'views_handler_filter_string') {
          $data[$table_name][$field_name]['filter']['handler'] = 'views_handler_filter_string_stripped';
        }
      }
    }
  }
}
