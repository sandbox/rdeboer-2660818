<?php

/**
 * @file views_filter_string_stripped.inc
 *
 * Definition of views_handler_filter_string_stripped.
 */

/**
 * Filter handler to implement stripping input prior to filtering/searching.
 *
 * @ingroup views_argument_handlers
 */
class views_handler_filter_string_stripped extends views_handler_filter_string {

  function option_definition() {
    $options = parent::option_definition();
    $options['expose']['contains']['trim_spaces'] = array('default' => FALSE, 'bool' => TRUE);
    return $options;
  }

  /**
   * Options subform to add checkbox to exposed filter options.
   */
  function expose_form(&$form, &$form_state) {
    parent::expose_form($form, $form_state);

    $form['expose']['trim_spaces'] = array(
      '#type' => 'checkbox',
      '#default_value' => $this->options['expose']['trim_spaces'],
      '#title' => t('Trim leading and trailing spaces'),
      '#description' => t('If checked, any blanks at the start or end of the input will be removed prior to filtering.'),
    );
  }
  
  /**
   * Check to see if input from the exposed filters should be stripped.
   */
  function accept_exposed_input($input) {
    if (!empty($this->options['expose']['trim_spaces']) && !empty($this->options['expose']['identifier'])) {
      $id = $this->options['expose']['identifier'];
      if (!empty($input[$id]) && is_string($input[$id])) {
        $input[$id] = trim($input[$id]);
      }
    }
    return parent::accept_exposed_input($input);
  }
}
